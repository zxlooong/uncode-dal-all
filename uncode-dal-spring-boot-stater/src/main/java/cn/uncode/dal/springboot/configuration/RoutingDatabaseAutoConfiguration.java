package cn.uncode.dal.springboot.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.uncode.dal.datasource.DynamicDataSource;
import cn.uncode.dal.interceptor.RoutingDatabaseInterceptor;
import cn.uncode.dal.mybatis.interceptor.DBSelectorInterceptor;

@Configuration
@ConditionalOnClass(DynamicDataSource.class)
public class RoutingDatabaseAutoConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RoutingDatabaseAutoConfiguration.class);
	
	@Bean(name="routingDatabaseInterceptor")
	public RoutingDatabaseInterceptor routingDatabaseInterceptor(){
		RoutingDatabaseInterceptor routingDatabaseInterceptor = new RoutingDatabaseInterceptor();
		LOGGER.info("===Uncode-Dal===RoutingDatabaseAutoConfiguration===>RoutingDatabase inited..");
		return routingDatabaseInterceptor;
	}
	
	@Bean
	public BeanNameAutoProxyCreator beanNameAutoProxyCreator(){
		BeanNameAutoProxyCreator beanNameAutoProxyCreator = new BeanNameAutoProxyCreator();
		beanNameAutoProxyCreator.setInterceptorNames("routingDatabaseInterceptor");
		beanNameAutoProxyCreator.setBeanNames(new String[]{"*DALImpl"});
		LOGGER.info("===Uncode-Dal===RoutingDatabaseAutoConfiguration===>RoutingDatabase registed");
		return beanNameAutoProxyCreator;
	}
	
	@Bean(name = "dBSelectorInterceptor")
	public DBSelectorInterceptor dbSelectorInterceptor(){
		LOGGER.info("===Uncode-Dal===RoutingDatabaseAutoConfiguration===>DBSelectorInterceptor inited..");
		return new DBSelectorInterceptor();
	}

}

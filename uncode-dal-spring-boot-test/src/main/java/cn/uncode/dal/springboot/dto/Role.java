package cn.uncode.dal.springboot.dto;

import java.io.Serializable;

import cn.uncode.dal.core.BaseDTO;

public class Role extends BaseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}

package cn.uncode.dal.springboot.biz.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.uncode.dal.annotation.Database;
import cn.uncode.dal.core.BaseDTO;
import cn.uncode.dal.criteria.QueryCriteria;
import cn.uncode.dal.criteria.QueryCriteria.Criteria;
import cn.uncode.dal.springboot.biz.UserService;
import cn.uncode.dal.springboot.dal.DictionarymappingDAL;
import cn.uncode.dal.springboot.dal.UserDAL;
import cn.uncode.dal.springboot.dto.DemoA;
import cn.uncode.dal.springboot.dto.DemoB;
import cn.uncode.dal.springboot.dto.Dictionarymapping;
import cn.uncode.dal.springboot.dto.Group;
import cn.uncode.dal.springboot.dto.Role;
import cn.uncode.dal.springboot.dto.User;

/**
 * Created by KevinBlandy on 2017/2/28 15:10
 */
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	@Lazy
	private UserDAL userDAL;
	
	
	@Autowired
	private DictionarymappingDAL dictionarymappingDAL;
	
	
	@Transactional
	public Integer create(User userEntity){
		User ur2 = new User();
		ur2.setName("aaaaaaaaa");
		Group g = new Group();
		g.setName("group11");
		Role r = new Role();
		r.setName("role333");
		g.setRole(r);
		ur2.setGroup(g);
		DemoB b = new DemoB();
		b.setName("bbbbb");
		g.setDemoB(b);
		DemoA a = new DemoA();
		a.setName("aaaaaaaaa");
		b.setDemoA(a);
		userDAL.saveEntity(ur2);
		return 1;
	}

	@Override
	public User users() {
		return userDAL.user();
	}


	@Override
	public User get(int id) {
//		User u = userDAL.getEntityById(15L);
		QueryCriteria queryCriteria =new QueryCriteria();
		queryCriteria.createCriteria().andColumnEqualTo(Dictionarymapping.STATUS, "EBL");
		List<Dictionarymapping> list = dictionarymappingDAL.getListByCriteria(queryCriteria);
		return null;
	}

//	@Transactional
	@Override
	@Database(custom="user")
	public void transaction() {
		QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.setTable("user");
		Criteria criteria = queryCriteria.createCriteria();
		List<Long> ids = new ArrayList<Long>();
		ids.add(92222l);
		ids.add(93333l);
		criteria.andColumnNotIn(BaseDTO.ID, ids);
		List<User>  list = userDAL.getListByCriteria(queryCriteria);
		System.out.println(list);
		
	}

	@Override
	public void update(User userEntity) {
//		User ur2 = new User();
////		ur2.setPid(2354432525L);
//		ur2.setAge(99);
////		ur2.setName("bbbbbbb");
//		QueryCriteria queryCriteria = new QueryCriteria();
//		queryCriteria.createCriteria().andColumnEqualTo("name", "bbbbbbb");
//		userDAL.updateEntityByCriteria(ur2, queryCriteria);
		Map<String, Object> map = new HashMap<>();
		map.put("contactWay", "aaaaaa");
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable("userauthentication");
        queryCriteria.createCriteria().andColumnEqualTo("userId", 12030).andColumnEqualTo("contactType",0);
        userDAL.getBaseDAL().updateByCriteria(map, queryCriteria);
		
	}
	
	@Override
	public void delete(int id) {
//		User ur2 = new User();
//		ur2.setId(13L);
//		ur2.setName("aaaaaaaaa");
//		Group g = new Group();
//		g.setId(5L);
//		g.setName("group11");
//		Role r = new Role();
//		r.setId(10L);
//		r.setName("role333");
//		g.setRole(r);
//		ur2.setGroup(g);
//		DemoB b = new DemoB();
//		b.setId(4);
//		b.setName("bbbbb");
//		g.setDemoB(b);
//		DemoA a = new DemoA();
//		a.setName("aaaaaaaaa");
//		a.setId(15);
//		b.setDemoA(a);
		userDAL.deleteEntityById(13L);
		
	}
	
	@Override
	public Integer addList(List<User> users) {
		List<User> us = new ArrayList<User>();
		User ur2 = new User();
		ur2.setAge(98);
		ur2.setName("aaaaaaaaa");
		us.add(ur2);
		User ur3 = new User();
		ur3.setAge(97);
		ur3.setName("zzzzzzzzzz");
		us.add(ur3);
		return (Integer) userDAL.insertEntity(ur2);
	}
}

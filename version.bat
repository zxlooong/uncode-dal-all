@echo on
@echo =============================================================
@echo $                                                           $
@echo $                     Uncode DAL                    $
@echo $                                                           $
@echo $                                                           $
@echo $                                                           $
@echo $  Uncode Studio All Right Reserved                         $
@echo $  Copyright (C) 2017-2050                                  $
@echo $  Author Yeweijun                                          $
@echo $                                                           $
@echo =============================================================
@echo.
@echo off

@title Uncode DAL
@color 0a

call mvn versions:set -DnewVersion=2.2.5

pause
package cn.uncode.dal.router;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TableRouter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tableName;
	
	private SharingType sharingType;
	
	private String fieldName;
	
	private List<Range> ranges = new ArrayList<Range>();
	
	private String clazz;
	
	private Set<String> indexTableFields = new HashSet<String>();

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public SharingType getSharingType() {
		return sharingType;
	}

	public void setSharingType(SharingType sharingType) {
		this.sharingType = sharingType;
	}

	public List<Range> getRanges() {
		return ranges;
	}

	public void setRanges(List<Range> ranges) {
		this.ranges = ranges;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public Set<String> getIndexTableFields() {
		return indexTableFields;
	}

	public void setIndexTableFields(String...tableFields){
		if(tableFields != null){
			for(String fd:tableFields){
				indexTableFields.add(fd);
			}
		}
	}
	
	public boolean hasIndexTableFields(){
		return indexTableFields.size() > 0;
	}
	

}

package cn.uncode.dal.router.strategy;

import java.util.Map;
/**
 * 自定义分表策略接口
 * @author juny
 *
 */
public interface ShardingStrategy {
	
	/**
	 * 根据表名查询所有分表
	 * @param tablbName 表名
	 * @return 分表集膈
	 */
	String[] selectAllShard(String tablbName);
	
	/**
	 * 根据指定字段值生成数据id
	 * @param field 字段名称
	 * @param obj 数据对象
	 * @return id
	 */
	long createShardId(String field, Map<String, Object> obj);
	
	/**
	 * 根据表名和id查找对应分表名称
	 * @param tablbName 表名
	 * @param id 记录id
	 * @return 分表名称
	 */
	String selectShardFromId(String tablbName, long id);


	/**
	 * 根据表名和分表字段查找对应分表名称
	 * @param tablbName 表名
	 * @param obj 字段值
	 * @return
	 */
	String selectShardFromField(String tablbName, Object obj);

	
}

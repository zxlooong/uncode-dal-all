package cn.uncode.dal.descriptor;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cn.uncode.dal.utils.BeanUtil;

public class ForeignKey implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 5675310807548144310L;
    
    private String fdName;
    
    private String tbName;
    
    private String fdTableName;
    
    private Class<?> fdClass;
    
    private Object id;
    
    private Map<String, Object> params;

    private Set<ForeignKey> foreignKeys = new HashSet<>();
    
    public ForeignKey() {
//    	this(null);
    }
    
//    public ForeignKey(ForeignKey foreignKey) {
//    	if(null != foreignKey) {
//    		this.fdName = foreignKey.getFdName();
//        	this.tbName = foreignKey.getTbName();
//        	this.fdClass = foreignKey.getFdClass();
//        	this.foreignKeys = foreignKey.getForeignKeys();
//        	this.id = foreignKey.getId();
//    	}
//    }

	public String getFdName() {
		return fdName;
	}

	public void setFdName(String fdName) {
		this.fdName = fdName;
	}

	public String getTbName() {
		return tbName;
	}

	public void setTbName(String tbName) {
		this.tbName = tbName;
	}

	public Class<?> getFdClass() {
		return fdClass;
	}

	public void setFdClass(Class<?> fdClass) {
		this.fdClass = fdClass;
	}

	public void addForeignKey(ForeignKey foreignKey) {
		foreignKeys.add(foreignKey);
	}
	
	public void addForeignKeys(Set<ForeignKey> foreignKeys) {
		foreignKeys.addAll(foreignKeys);
	}
	
	public Set<ForeignKey> getForeignKeys() {
		return foreignKeys;
	}
    
	
	public boolean hasForeignKey() {
		return foreignKeys.size() > 0;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
	public String getFdTableName() {
		return fdTableName;
	}

	public void setFdTableName(String fdTableName) {
		this.fdTableName = fdTableName;
	}

	public String toString() {
		return BeanUtil.objToMap(this).toString();
	}
	
	
    

}

package cn.uncode.dal.annotation;

public enum DatabaseType {
	 /** 写库 */
	WRITE,

    /** 读库 */
	READ,

	/** 业务处理库，默认为写库，设置该库当前线程将不进行读写分离切换 */
	TRANSACTION
}

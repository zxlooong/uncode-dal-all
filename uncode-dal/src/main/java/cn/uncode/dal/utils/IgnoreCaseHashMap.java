package cn.uncode.dal.utils;

import java.util.LinkedHashMap;

public class IgnoreCaseHashMap<K, V> extends LinkedHashMap<K, V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6837240986692088738L;
    @Override
	public boolean containsKey(Object key) {
		boolean rt = false;
		String strKey = String.valueOf(key);
		for(Object item:this.keySet()) {
			String itemKey = String.valueOf(item);
			if(itemKey.toLowerCase().equals(strKey.toLowerCase())){
				rt = true;
				break;
			}
		}
		return rt;
    }
	@Override
	public V get(Object key) {
		V rt = null;
		String strKey = String.valueOf(key);
		for(java.util.Map.Entry<K, V> item:this.entrySet()) {
			String itemKey = String.valueOf(item.getKey());
			if(itemKey.toLowerCase().equals(strKey.toLowerCase())){
				rt = item.getValue();
				break;
			}
		}
        return rt;
    }

}

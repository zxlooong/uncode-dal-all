package cn.uncode.dal.asyn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.uncode.dal.criteria.QueryCriteria;
import org.apache.commons.lang3.StringUtils;

public class AsynContext extends HashMap<String, Object> {

	private static final String CONTEXT_QUERY_CRITERIA = "_queryCriteria";
	private static final String CONTEXT_FILED = "_filed";
	private static final String CONTEXT_DATABASE = "_database";
	private static final String CONTEXT_MAP = "_map";
	private static final String CONTEXT_TABLE = "_table";
	private static final String CONTEXT_OBJ = "_obj";
	private static final String CONTEXT_METHOD = "_method";
	/**
	 * 
	 */
	private static final long serialVersionUID = -7574370441086237881L;
	
	public AsynContext(Method method, Object obj){
		this.put(CONTEXT_METHOD, method);
		this.put(CONTEXT_OBJ, obj);
	}
	
	public AsynContext(Method method, String table, Map<String, Object> obj){
		this.put(CONTEXT_METHOD, method);
		this.put(CONTEXT_TABLE, table);
		this.put(CONTEXT_MAP, obj);
	}
	
	public AsynContext(Method method, String database, String table, Map<String, Object> obj){
		this.put(CONTEXT_METHOD, method);
		this.put(CONTEXT_DATABASE, database);
		this.put(CONTEXT_TABLE, table);
		this.put(CONTEXT_MAP, obj);
	}
	
	public Object getObj(){
		return this.get(CONTEXT_OBJ);
	}
	
	public String getTable(){
		return (String) this.get(CONTEXT_TABLE);
	}
	
	public void setTable(String table){
		this.put(CONTEXT_TABLE, table);
	}
	
	public void setFields(List<String> list){
		this.put(CONTEXT_FILED, list);
	}

	public void setDatabase(String database){
		if(StringUtils.isNotBlank(database)){
			this.put(CONTEXT_DATABASE,database);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getFields(){
		return (List<String>) this.get(CONTEXT_FILED);
	}
	
	public String getDatabase(){
		return (String) this.get(CONTEXT_DATABASE);
	}
	
	public Method getMethod(){
		return (Method) this.get(CONTEXT_METHOD);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getMapObj(){
		return (Map<String, Object>) this.get(CONTEXT_MAP);
	}
	
	public void setQueryCriteria(QueryCriteria queryCriteria){
		this.put(CONTEXT_QUERY_CRITERIA, queryCriteria);
		
	}
	
	public QueryCriteria getQueryCriteria(){
		return (QueryCriteria) this.get(CONTEXT_QUERY_CRITERIA);
	}



}
